#include <getopt.h>
#include <math.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../lib/libjson/json.h"
#include "../lib/cresead/src/cresead.h"
#include "../lib/stb/stretchy_buffer.h"

#include "utils.h"
#include "cacturn.h"

static void _turnip_predictions_destroy(prediction_t*** _sb_result);
static prediction_t* _turnip_generate_pattern_0_with_length(turnip_price_t* turnip,
                                                            int hi_phase_len_1,
                                                            int dec_phase_len_1,
                                                            int hi_phase_len_2,
                                                            int dec_phase_len_2,
                                                            int hi_phase_len_3);
static prediction_t* _turnip_generate_pattern_1_with_peak(turnip_price_t* turnip,
                                                          int peak_start);
static prediction_t* _turnip_generate_pattern_2(turnip_price_t* turnip);
static prediction_t* _turnip_generate_pattern_3_with_peak(turnip_price_t* turnip,
                                                          int peak_start);
static void _turnip_prediction_set_min_max(prediction_t* prediction);
static char* _turnip_prediction_string(prediction_t* predictions);
static char* _turnip_json_string(turnip_price_t* turnip);
static int _turnip_print_json_callback(void* data, const char* s, uint32_t len);


char* PRICE_PERIOD_KEYS[] = {
    [PRICE_PERIOD_JSON_KEY_MIN] = "min",
    [PRICE_PERIOD_JSON_KEY_MAX] = "max"
};

char* PREDICTION_KEYS[] = {
    [PREDICTION_JSON_KEY_PATTERN_NAME] = "pattern_name",
    [PREDICTION_JSON_KEY_PERIOD]       = "period",
    [PREDICTION_JSON_KEY_MIN]          = "min",
    [PREDICTION_JSON_KEY_MAX]          = "max"
};

char* TURNIP_KEYS[] = {
    [TURNIP_JSON_KEY_BASE_PRICE]  = "base_price",
    [TURNIP_JSON_KEY_SELL_PRICE]  = "sell_price",
    [TURNIP_JSON_KEY_PATTERN]     = "pattern",
    [TURNIP_JSON_KEY_PREDICTIONS] = "predictions"
};


int
main(int argc, char** argv)
{
    sead_random_t* rand;
    turnip_price_t* turnip;
    uint32_t seed;

    options_t opt = {
        .op = OPERATE_QUERY,
        .view = VIEW_NORMAL,
        .pattern = FILTER_NONE
    };

    if (argc < 2)
    {
        usage(argv[0]);
        exit(1);
    }

    parse_options(argc, argv, &opt);
    turnip = turnip_new();

    if (opt.op == OPERATE_QUERY)
    {
        if (! bit_set(opt.pattern & FILTER_ACTIVE, 5) && opt.pattern == FILTER_NONE)
            opt.pattern = FILTER_ALL;

        if (turnip == NULL)
            exit(1);

        turnip->base_price = atoi(argv[optind++]);

        if (out_of_range(turnip->base_price, 90, 110))
        {
            fprintf(stderr, "Error: Turnip price needs to be between 90 and 110\n");
            exit(1);
        }

        memset(turnip->sell_price, -1, 14 * sizeof(int16_t));
        for (int count = 2; optind < argc && count < 14; optind += (count++, 1))
            turnip->sell_price[count] = atoi(argv[optind]);

        if (bit_set(opt.pattern & FILTER_FLUCTUATING, 1))
            turnip_generate_pattern_0(turnip);
        if (bit_set(opt.pattern & FILTER_LARGE_SPIKE, 2))
            turnip_generate_pattern_1(turnip);
        if (bit_set(opt.pattern & FILTER_DECREASING, 3))
            turnip_generate_pattern_2(turnip);
        if (bit_set(opt.pattern & FILTER_SMALL_SPIKE, 4))
            turnip_generate_pattern_3(turnip);

        switch (opt.view)
        {
            case VIEW_JSON:
                turnip_print_json(turnip);
                break;

            case VIEW_NORMAL: default:
                turnip_print_predictions(turnip);
                break;
        }
    }
    else if (opt.op == OPERATE_GENERATE)
    {
        for (; optind < argc; optind++)
        {
            seed = atoi(argv[optind]);
            rand = sead_random_init(seed);
            turnip_calculate_price(turnip, rand);
            printf("Seed: %d\n", seed);
            turnip_print_prices(turnip);
            sead_random_destroy(&rand);
        }
    }

    turnip_destroy(&turnip);

    return 0;
}


void
usage(char* prog)
{
    fprintf(stderr,
            "Usage: %s { [-g seed [seed...]] | [-j] [-p 0-3] [buy price] [sell prices] }\n",
            prog);
}


void
parse_options(int argc, char** argv, options_t* opt)
{
    int i;
    while ((i = getopt(argc, argv, "gjp:h")) != -1)
    {
        switch (i)
        {
            case 'g':
                opt->op = OPERATE_GENERATE;
                break;

            case 'j':
                opt->view = VIEW_JSON;
                break;
            case 'p':
                opt->pattern |= FILTER_ACTIVE;
                switch (atoi(optarg))
                {
                    case 0: opt->pattern |= FILTER_FLUCTUATING; break;
                    case 1: opt->pattern |= FILTER_LARGE_SPIKE; break;
                    case 2: opt->pattern |= FILTER_DECREASING; break;
                    case 3: opt->pattern |= FILTER_SMALL_SPIKE; break;
                    default:
                        usage(argv[0]);
                        exit(1);
                        break;
                }
                break;
            case 'h':
                usage(argv[0]);
                exit(0);
            default:
                usage(argv[0]);
                exit(1);
        }
    }
}

turnip_price_t*
turnip_new(void)
{
    turnip_price_t* turnip;
    turnip = calloc(1, sizeof(turnip_price_t));
    if (turnip == NULL)
        goto mem_error;

    return turnip;

mem_error:
    fprintf(stderr, "Out of Memory!\n");
    return NULL;
}


void
turnip_destroy(turnip_price_t** _turnip)
{
    turnip_price_t* turnip;
    if (*_turnip != NULL)
    {
        turnip = *_turnip;

        _turnip_predictions_destroy(&(turnip->predictions[PATTERN_FLUCTUATING]));
        _turnip_predictions_destroy(&(turnip->predictions[PATTERN_SMALL_SPIKE]));
        _turnip_predictions_destroy(&(turnip->predictions[PATTERN_LARGE_SPIKE]));
        _turnip_predictions_destroy(&(turnip->predictions[PATTERN_DECREASING]));

        free(turnip);
        *_turnip = NULL;
    }
}


static void
_turnip_predictions_destroy(prediction_t*** _sb_result)
{
    prediction_t** sb_result;
    if (*_sb_result != NULL)
    {
        sb_result = *_sb_result;
        for (int i = 0; i < sb_count(sb_result); i++)
            free(sb_result[i]);
        sb_free(sb_result);
        *_sb_result = NULL;
    }
}


void
turnip_calculate_price(turnip_price_t* turnip, sead_random_t* rand)
{
    // Turnip price calculator adapted from Ninji
    // https://gist.github.com/Treeki/85be14d297c80c8b3c0a76375743325b
    void (*turnip_patterns[4])(turnip_price_t*, sead_random_t*) = {
        turnip_calculate_pattern_0,
        turnip_calculate_pattern_1,
        turnip_calculate_pattern_2,
        turnip_calculate_pattern_3
    };

    turnip_setup_next_price(turnip, rand);
    turnip_patterns[turnip->pattern](turnip, rand);

    turnip->sell_price[0] = 0;
    turnip->sell_price[1] = 0;
}


void
turnip_calculate_pattern_0(turnip_price_t* turnip, sead_random_t* rand)
{
    int work;
    int dec_phase_len_1;
    int dec_phase_len_2;
    int hi_phase_len_1;
    int hi_phase_len_2_3;
    int hi_phase_len_3;
    float rate;

    work = 2;
    dec_phase_len_1 = randbool(rand) ? 3 : 2;
    dec_phase_len_2 = 5 - dec_phase_len_1;

    hi_phase_len_1 = randint(rand, 0, 6);
    hi_phase_len_2_3 = 7 - hi_phase_len_1;
    hi_phase_len_3 = randint(rand, 0, hi_phase_len_2_3 - 1);

    // high phase 1
    for (int i = 0; i < hi_phase_len_1; i++)
        turnip->sell_price[work++] = intceil(randfloat(rand, 0.9, 1.4) * turnip->base_price);

    // decreasing phase 1
    rate = randfloat(rand, 0.8, 0.6);
    for (int i = 0; i < dec_phase_len_1; i++)
    {
        turnip->sell_price[work++] = intceil(rate * turnip->base_price);
        rate -= 0.04;
        rate -= randfloat(rand, 0, 0.06);
    }

    // high phase 2
    for (int i = 0; i < (hi_phase_len_2_3 - hi_phase_len_3); i++)
        turnip->sell_price[work++] = intceil(randfloat(rand, 0.9, 1.4) * turnip->base_price);

    // decreasing phase 2
    rate = randfloat(rand, 0.8, 0.6);
    for (int i = 0; i < dec_phase_len_2; i++)
    {
        turnip->sell_price[work++] = intceil(rate * turnip->base_price);
        rate -= 0.04;
        rate -= randfloat(rand, 0, 0.06);
    }

    // high phase 3
    for (int i = 0; i < hi_phase_len_3; i++)
        turnip->sell_price[work++] = intceil(randfloat(rand, 0.9, 1.4) * turnip->base_price);
}


void
turnip_calculate_pattern_1(turnip_price_t* turnip, sead_random_t* rand)
{
    int work;
    int peak_start;
    float rate;

    peak_start = randint(rand, 3, 9);
    rate = randfloat(rand, 0.9, 0.85);

    for (work = 2; work < peak_start; work++)
    {
        turnip->sell_price[work] = intceil(rate * turnip->base_price);
        rate -= 0.03;
        rate -= randfloat(rand, 0, 0.02);
    }

    turnip->sell_price[work++] = intceil(randfloat(rand, 0.9, 1.4) * turnip->base_price);
    turnip->sell_price[work++] = intceil(randfloat(rand, 1.4, 2.0) * turnip->base_price);
    turnip->sell_price[work++] = intceil(randfloat(rand, 2.0, 6.0) * turnip->base_price);
    turnip->sell_price[work++] = intceil(randfloat(rand, 1.4, 2.0) * turnip->base_price);
    turnip->sell_price[work++] = intceil(randfloat(rand, 0.9, 1.4) * turnip->base_price);

    for (; work < 14; work++)
        turnip->sell_price[work] = intceil(randfloat(rand, 0.4, 0.9) * turnip->base_price);
}


void
turnip_calculate_pattern_2(turnip_price_t* turnip, sead_random_t* rand)
{
    int work;
    float rate;

    rate = 0.9;
    rate -= randfloat(rand, 0, 0.05);

    for (work = 2; work < 14; work++)
    {
        turnip->sell_price[work] = intceil(rate * turnip->base_price);
        rate -= 0.03;
        rate -= randfloat(rand, 0, 0.02);
    }
}


void
turnip_calculate_pattern_3(turnip_price_t* turnip, sead_random_t* rand)
{
    int work;
    int peak_start;
    float rate;

    peak_start = randint(rand, 2, 9);
    rate = randfloat(rand, 0.9, 0.4);

    for (work = 2; work < peak_start; work++)
    {
        turnip->sell_price[work] = intceil(rate * turnip->base_price);
        rate -= 0.03;
        rate -= randfloat(rand, 0, 0.02);
    }

    turnip->sell_price[work++] = intceil(randfloat(rand, 0.9, 1.4) * (float)turnip->base_price);
    turnip->sell_price[work++] = intceil(randfloat(rand, 0.9, 1.4) * turnip->base_price);
    rate = randfloat(rand, 1.4, 2.0);
    turnip->sell_price[work++] = intceil(randfloat(rand, 1.4, rate) * turnip->base_price) - 1;
    turnip->sell_price[work++] = intceil(rate * turnip->base_price);
    turnip->sell_price[work++] = intceil(randfloat(rand, 1.4, rate) * turnip->base_price) - 1;

    rate = randfloat(rand, 0.9, 0.4);
    for (; work < 14; work++)
    {
        turnip->sell_price[work] = intceil(rate * turnip->base_price);
        rate -= 0.03;
        rate -= randfloat(rand, 0, 0.02);
    }
}


void
turnip_setup_next_price(turnip_price_t* turnip, sead_random_t* rand)
{
    turnip->base_price = randint(rand, 90, 110);
    int prob = randint(rand, 0, 99);
    int next;

    switch (turnip->pattern)
    {
        case 0: next = set_next_pattern_from_prob(prob, 20, 50, 65); break;
        case 1: next = set_next_pattern_from_prob(prob, 50, 55, 75); break;
        case 2: next = set_next_pattern_from_prob(prob, 25, 70, 75); break;
        case 3: next = set_next_pattern_from_prob(prob, 45, 70, 85); break;
        default: next = 2; break;
    }

    turnip->pattern = next;

    for (int i = 2; i < 14; i++)
        turnip->sell_price[i] = 0;
    turnip->sell_price[0] = turnip->base_price;
    turnip->sell_price[1] = turnip->base_price;
}


void
turnip_generate_pattern_0(turnip_price_t* turnip)
{
    // Turnip price predictor adapted from mikebryant
    // https://github.com/mikebryant/ac-nh-turnip-prices
    prediction_t* result;
    int limit;

    for (int dec_phase_len_1 = 2; dec_phase_len_1 < 4; dec_phase_len_1++)
    {
        for (int hi_phase_len_1 = 0; hi_phase_len_1 < 7; hi_phase_len_1++)
        {
            limit = 7 - hi_phase_len_1;
            for (int hi_phase_len_3 = 0; hi_phase_len_3 < limit; hi_phase_len_3++)
            {
                result = _turnip_generate_pattern_0_with_length(
                    turnip,
                    hi_phase_len_1,
                    dec_phase_len_1,
                    7 - hi_phase_len_1 - hi_phase_len_3,
                    5 - dec_phase_len_1,
                    hi_phase_len_3
                );

                if (result != NULL)
                {
                    strncpy(result->pattern_name, "Fluctuating", 12);
                    sb_push(turnip->predictions[PATTERN_FLUCTUATING], result);
                }
            }
        }
    }
}


void
turnip_generate_pattern_1(turnip_price_t* turnip)
{
    prediction_t* result;

    for (int peak_start = 3; peak_start < 10; peak_start++)
    {
        result = _turnip_generate_pattern_1_with_peak(turnip, peak_start);

        if (result != NULL)
        {
            strncpy(result->pattern_name, "Large Spike", 12);
            sb_push(turnip->predictions[PATTERN_LARGE_SPIKE], result);
        }
    }
}


void
turnip_generate_pattern_2(turnip_price_t* turnip)
{
    prediction_t* result;

    result = _turnip_generate_pattern_2(turnip);
    if (result != NULL)
    {
        strncpy(result->pattern_name, "Decreasing", 12);
        sb_push(turnip->predictions[PATTERN_DECREASING], result);
    }
}


void
turnip_generate_pattern_3(turnip_price_t* turnip)
{
    prediction_t* result;

    for (int peak_start = 2; peak_start < 10; peak_start++)
    {
        result = _turnip_generate_pattern_3_with_peak(turnip, peak_start);

        if (result != NULL)
        {
            strncpy(result->pattern_name, "Small Spike", 12);
            sb_push(turnip->predictions[PATTERN_SMALL_SPIKE], result);
        }
    }
}


static prediction_t*
_turnip_generate_pattern_0_with_length(turnip_price_t* turnip,
                                       int hi_phase_len_1,
                                       int dec_phase_len_1,
                                       int hi_phase_len_2,
                                       int dec_phase_len_2,
                                       int hi_phase_len_3)
{
    prediction_t* result;
    price_period_t predict;

    int buy_price = turnip->base_price;

    int min_rate;
    int max_rate;

    int start;
    int stop;

    result = calloc(1, sizeof(prediction_t));
    if (result == NULL)
        goto mem_error;

    // Setup result with buy price
    start = 0;
    stop = 2;
    for (int i = start; i < stop; i++)
    {
        predict.min = buy_price;
        predict.max = buy_price;
        result->period[i] = predict;
    }

    // high phase 1
    start = stop;
    stop += hi_phase_len_1;
    for (int i = start; i < stop; i++)
    {
        predict.min = (int)floor(0.9 * buy_price);
        predict.max = (int)ceil(1.4 * buy_price);

        if (turnip->sell_price[i] != -1)
        {
            // Out of range for this pattern
            if (out_of_range(turnip->sell_price[i], predict.min, predict.max))
                goto early_exit;

            predict.min = turnip->sell_price[i];
            predict.max = turnip->sell_price[i];
        }

        result->period[i] = predict;
    }

    // decreasing phase 1
    min_rate = 6000;
    max_rate = 8000;
    start = stop;
    stop += dec_phase_len_1;
    for (int i = start; i < stop; i++)
    {
        predict.min = (int)floor(min_rate * buy_price / 10000);
        predict.max = (int)ceil(max_rate * buy_price / 10000);

        if (turnip->sell_price[i] != -1)
        {
            // Out of range for this pattern
            if (out_of_range(turnip->sell_price[i], predict.min, predict.max))
                goto early_exit;

            predict.min = turnip->sell_price[i];
            predict.max = turnip->sell_price[i];
            min_rate = min_rate_from_sell_buy(turnip->sell_price[i], buy_price);
            max_rate = max_rate_from_sell_buy(turnip->sell_price[i], buy_price);
        }

        result->period[i] = predict;

        min_rate -= 1000;
        max_rate -= 400;
    }

    // high phase 2
    start = stop;
    stop += hi_phase_len_2;
    for (int i = start; i < stop; i++)
    {
        predict.min = (int)floor(0.9 * buy_price);
        predict.max = (int)ceil(1.4 * buy_price);

        if (turnip->sell_price[i] != -1)
        {
            // Out of range for this pattern
            if (out_of_range(turnip->sell_price[i], predict.min, predict.max))
                goto early_exit;

            predict.min = turnip->sell_price[i];
            predict.max = turnip->sell_price[i];
        }

        result->period[i] = predict;
    }

    // decreasing phase 2
    min_rate = 6000;
    max_rate = 8000;
    start = stop;
    stop += dec_phase_len_2;
    for (int i = start; i < stop; i++)
    {
        predict.min = (int)floor(min_rate * buy_price / 10000);
        predict.max = (int)ceil(max_rate * buy_price / 10000);

        if (turnip->sell_price[i] != -1)
        {
            // Out of range for this pattern
            if (out_of_range(turnip->sell_price[i], predict.min, predict.max))
                goto early_exit;

            predict.min = turnip->sell_price[i];
            predict.max = turnip->sell_price[i];
            min_rate = min_rate_from_sell_buy(turnip->sell_price[i], buy_price);
            max_rate = max_rate_from_sell_buy(turnip->sell_price[i], buy_price);
        }

        result->period[i] = predict;

        min_rate -= 1000;
        max_rate -= 400;
    }

    // high phase 3
    start = stop;
    stop += hi_phase_len_3;
    if (stop != 14)
        goto early_exit;

    for (int i = start; i < stop; i++)
    {
        predict.min = (int)floor(0.9 * buy_price);
        predict.max = (int)ceil(1.4 * buy_price);

        if (turnip->sell_price[i] != -1)
        {
            // Out of range for this pattern
            if (out_of_range(turnip->sell_price[i], predict.min, predict.max))
                goto early_exit;

            predict.min = turnip->sell_price[i];
            predict.max = turnip->sell_price[i];
        }

        result->period[i] = predict;
    }

    _turnip_prediction_set_min_max(result);

    return result;

early_exit:
    free(result);
    return NULL;

mem_error:
    fprintf(stderr, "Out of Memory!\n");
    return NULL;
}


static prediction_t*
_turnip_generate_pattern_1_with_peak(turnip_price_t* turnip, int peak_start)
{
    prediction_t* result = NULL;
    price_period_t predict;

    int buy_price = turnip->base_price;

    int min_rate;
    int max_rate;

    float min_rand[11] = {
        0.9f, 1.4f, 2.0f, 1.4f, 0.9f, 0.4f,
        0.4f, 0.4f, 0.4f, 0.4f, 0.4f
    };

    float max_rand[11] = {
        1.4f, 2.0f, 6.0f, 2.0f, 1.4f, 0.9f,
        0.9f, 0.9f, 0.9f, 0.9f, 0.9f
    };

    result = calloc(1, sizeof(prediction_t));
    if (result == NULL)
        goto mem_error;

    // Setup result with buy price
    for (int i = 0; i < 2; i++)
    {
        predict.min = buy_price;
        predict.max = buy_price;
        result->period[i] = predict;
    }

    min_rate = 8500;
    max_rate = 9000;
    for (int i = 2; i < peak_start; i++)
    {
        predict.min = (int)floor(min_rate * buy_price / 10000);
        predict.max = (int)ceil(max_rate * buy_price / 10000);

        if (turnip->sell_price[i] != -1)
        {
            // Out of range for this pattern
            if (out_of_range(turnip->sell_price[i], predict.min, predict.max))
                goto early_exit;

            predict.min = turnip->sell_price[i];
            predict.max = turnip->sell_price[i];
            min_rate = min_rate_from_sell_buy(turnip->sell_price[i], buy_price);
            max_rate = max_rate_from_sell_buy(turnip->sell_price[i], buy_price);
        }

        result->period[i] = predict;

        min_rate -= 500;
        max_rate -= 300;
    }

    for (int i = peak_start; i < 14; i++)
    {
        predict.min = (int)floor(min_rand[i - peak_start] * buy_price);
        predict.max = (int)ceil(max_rand[i - peak_start] * buy_price);

        if (turnip->sell_price[i] != -1)
        {
            // Out of range for this pattern
            if (out_of_range(turnip->sell_price[i], predict.min, predict.max))
                goto early_exit;

            predict.min = turnip->sell_price[i];
            predict.max = turnip->sell_price[i];
        }

        result->period[i] = predict;
    }

    _turnip_prediction_set_min_max(result);

    return result;

early_exit:
    free(result);
    return NULL;

mem_error:
    fprintf(stderr, "Out of Memory!\n");
    return NULL;
}


static prediction_t*
_turnip_generate_pattern_2(turnip_price_t* turnip)
{
    prediction_t* result;
    price_period_t predict;

    int buy_price = turnip->base_price;

    int min_rate;
    int max_rate;

    result = calloc(1, sizeof(prediction_t));
    if (result == NULL)
        goto mem_error;

    // Setup result with buy price
    for (int i = 0; i < 2; i++)
    {
        predict.min = buy_price;
        predict.max = buy_price;
        result->period[i] = predict;
    }

    min_rate = 8500;
    max_rate = 9000;
    for (int i = 2; i < 14; i++)
    {
        predict.min = (int)floor(min_rate * buy_price / 10000);
        predict.max = (int)ceil(max_rate * buy_price / 10000);

        if (turnip->sell_price[i] != -1)
        {
            // Out of range for this pattern
            if (out_of_range(turnip->sell_price[i], predict.min, predict.max))
                goto early_exit;

            predict.min = turnip->sell_price[i];
            predict.max = turnip->sell_price[i];
            min_rate = min_rate_from_sell_buy(turnip->sell_price[i], buy_price);
            max_rate = max_rate_from_sell_buy(turnip->sell_price[i], buy_price);
        }

        result->period[i] = predict;

        min_rate -= 500;
        max_rate -= 300;
    }

    _turnip_prediction_set_min_max(result);

    return result;

early_exit:
    free(result);
    return NULL;

mem_error:
    fprintf(stderr, "Out of Memory!\n");
    return NULL;
}


static prediction_t* _turnip_generate_pattern_3_with_peak(turnip_price_t* turnip,
                                                          int peak_start)
{
    prediction_t* result = NULL;
    price_period_t predict;

    int buy_price = turnip->base_price;

    int min_rate;
    int max_rate;

    result = calloc(1, sizeof(prediction_t));
    if (result == NULL)
        goto mem_error;

    // Setup result with buy price
    for (int i = 0; i < 2; i++)
    {
        predict.min = buy_price;
        predict.max = buy_price;
        result->period[i] = predict;
    }

    min_rate = 4000;
    max_rate = 9000;

    for (int i = 2; i < peak_start; i++)
    {
        predict.min = (int)floor(min_rate * buy_price / 10000);
        predict.max = (int)ceil(max_rate * buy_price / 10000);

        if (turnip->sell_price[i] != -1)
        {
            // Out of range for this pattern
            if (out_of_range(turnip->sell_price[i], predict.min, predict.max))
                goto early_exit;

            predict.min = turnip->sell_price[i];
            predict.max = turnip->sell_price[i];
            min_rate = min_rate_from_sell_buy(turnip->sell_price[i], buy_price);
            max_rate = max_rate_from_sell_buy(turnip->sell_price[i], buy_price);
        }

        result->period[i] = predict;

        min_rate -= 500;
        max_rate -= 300;
    }

    // Peak
    for (int i = peak_start; i < peak_start + 2; i++)
    {
        predict.min = (int)floor(0.9 * buy_price);
        predict.max = (int)ceil(1.4 * buy_price);

        if (turnip->sell_price[i] != -1)
        {
            // Out of range for this pattern
            if (out_of_range(turnip->sell_price[i], predict.min, predict.max))
                goto early_exit;

            predict.min = turnip->sell_price[i];
            predict.max = turnip->sell_price[i];
        }

        result->period[i] = predict;
    }

    // Main Spike 1
    predict.min = (int)floor(1.4 * buy_price) - 1;
    predict.max = (int)ceil(2.0 * buy_price) - 1;

    if (turnip->sell_price[peak_start + 2] != -1)
    {
        // Out of range for this pattern
        if (out_of_range(turnip->sell_price[peak_start + 2], predict.min, predict.max))
            goto early_exit;

        predict.min = turnip->sell_price[peak_start + 2];
        predict.max = turnip->sell_price[peak_start + 2];
    }

    result->period[peak_start + 2] = predict;

    // Main Spike 2
    predict.min = result->period[peak_start + 2].min;
    predict.max = (int)ceil(2.0 * buy_price);

    if (turnip->sell_price[peak_start + 3] != -1)
    {
        // Out of range for this pattern
        if (out_of_range(turnip->sell_price[peak_start + 3], predict.min, predict.max))
            goto early_exit;

        predict.min = turnip->sell_price[peak_start + 3];
        predict.max = turnip->sell_price[peak_start + 3];
    }

    result->period[peak_start + 3] = predict;

    // Main Spike 3
    predict.min = (int)floor(1.4 * buy_price) - 1;
    predict.max = result->period[peak_start + 3].max - 1;

    if (turnip->sell_price[peak_start + 4] != -1)
    {
        // Out of range for this pattern
        if (out_of_range(turnip->sell_price[peak_start + 4], predict.min, predict.max))
            goto early_exit;

        predict.min = turnip->sell_price[peak_start + 4];
        predict.max = turnip->sell_price[peak_start + 4];
    }

    result->period[peak_start + 4] = predict;

    if ((peak_start + 5) < 14)
    {
        min_rate = 4000;
        max_rate = 9000;

        for (int i = peak_start + 5; i < 14; i++)
        {
            predict.min = (int)floor(min_rate * buy_price / 10000);
            predict.max = (int)ceil(max_rate * buy_price / 10000);

            if (turnip->sell_price[i] != -1)
            {
                // Out of range for this pattern
                if (out_of_range(turnip->sell_price[i], predict.min, predict.max))
                    goto early_exit;

                predict.min = turnip->sell_price[i];
                predict.max = turnip->sell_price[i];
                min_rate = min_rate_from_sell_buy(turnip->sell_price[i], buy_price);
                max_rate = max_rate_from_sell_buy(turnip->sell_price[i], buy_price);
            }

            result->period[i] = predict;

            min_rate -= 500;
            max_rate -= 300;
        }
    }

    _turnip_prediction_set_min_max(result);

    return result;

early_exit:
    free(result);
    return NULL;

mem_error:
    fprintf(stderr, "Out of Memory!\n");
    return NULL;
}


static void
_turnip_prediction_set_min_max(prediction_t* p)
{
    for (int i = 2; i < 14; i++)
    {
        p->min = p->period[i].min > p->min ? p->period[i].min : p->min;
        p->max = p->period[i].max > p->max ? p->period[i].max : p->max;
    }
}


void turnip_print_prices(turnip_price_t* turnip)
{
    printf("Pattern %d:\n", turnip->pattern);
    printf("Sun  Mon  Tue  Wed  Thr  Fri  Sat\n");
    printf("%3d  %3d  %3d  %3d  %3d  %3d  %3d\n",
            turnip->base_price,
            turnip->sell_price[MONDAY_AM],
            turnip->sell_price[TUESDAY_AM],
            turnip->sell_price[WEDNESDAY_AM],
            turnip->sell_price[THURSDAY_AM],
            turnip->sell_price[FRIDAY_AM],
            turnip->sell_price[SATURDAY_AM]);
    printf("     %3d  %3d  %3d  %3d  %3d  %3d\n",
            turnip->sell_price[MONDAY_PM],
            turnip->sell_price[TUESDAY_PM],
            turnip->sell_price[WEDNESDAY_PM],
            turnip->sell_price[THURSDAY_PM],
            turnip->sell_price[FRIDAY_PM],
            turnip->sell_price[SATURDAY_PM]);
}


void turnip_print_predictions(turnip_price_t* turnip)
{
    printf("\t\tMonday\t\tTuesday\t\tWednesday\tThursday\tFriday\t\tSaturday\tMin-Max\n");
    printf("\t\t" "AM\tPM\t" "AM\tPM\t" "AM\tPM\t" "AM\tPM\t" "AM\tPM\t" "AM\tPM\n");
    for (int i = 0; i < NUM_PATTERNS; i++)
        for (int j = 0; j < sb_count(turnip->predictions[i]); j++)
                printf("%s\n", _turnip_prediction_string(turnip->predictions[i][j]));
}


void
turnip_print_json(turnip_price_t* turnip)
{
    printf("%s\n", _turnip_json_string(turnip));
}


static char*
_turnip_prediction_string(prediction_t* predictions)
{
    static char buf[BUFSIZ];
    snprintf(buf, BUFSIZ, TURNIP_PREDICTION_WEEK_FORMAT,
             predictions->pattern_name,
             predictions->period[MONDAY_AM].min, predictions->period[MONDAY_AM].max,
             predictions->period[MONDAY_PM].min, predictions->period[MONDAY_PM].max,
             predictions->period[TUESDAY_AM].min, predictions->period[TUESDAY_AM].max,
             predictions->period[TUESDAY_PM].min, predictions->period[TUESDAY_PM].max,
             predictions->period[WEDNESDAY_AM].min, predictions->period[WEDNESDAY_AM].max,
             predictions->period[WEDNESDAY_PM].min, predictions->period[WEDNESDAY_PM].max,
             predictions->period[THURSDAY_AM].min, predictions->period[THURSDAY_AM].max,
             predictions->period[THURSDAY_PM].min, predictions->period[THURSDAY_PM].max,
             predictions->period[FRIDAY_AM].min, predictions->period[FRIDAY_AM].max,
             predictions->period[FRIDAY_PM].min, predictions->period[FRIDAY_PM].max,
             predictions->period[SATURDAY_AM].min, predictions->period[SATURDAY_AM].max,
             predictions->period[SATURDAY_PM].min, predictions->period[SATURDAY_PM].max,
             predictions->min, predictions->max);
    return buf;
}


static char*
_turnip_json_string(turnip_price_t* turnip)
{
    json_printer jp;
    static char buf[BUFSIZ * BUFSIZ];

    memset(buf, '\0', BUFSIZ * BUFSIZ);
    json_print_init(&jp, _turnip_print_json_callback, buf);

    json_print_pretty(&jp, JSON_OBJECT_BEGIN, NULL, 0);

    _json_format_key_int(&jp, TURNIP_KEYS[TURNIP_JSON_KEY_BASE_PRICE],
                              turnip->base_price);
    _json_format_key_int16_arr(&jp, TURNIP_KEYS[TURNIP_JSON_KEY_SELL_PRICE],
                                    turnip->sell_price, 14);
    _json_format_key_int(&jp, TURNIP_KEYS[TURNIP_JSON_KEY_PATTERN],
                              turnip->pattern);

    _json_format_key(&jp, TURNIP_KEYS[TURNIP_JSON_KEY_PREDICTIONS]);
    json_print_pretty(&jp, JSON_ARRAY_BEGIN, NULL, 0);
    for (int i = 0; i < NUM_PATTERNS; i++)
    {
        json_print_pretty(&jp, JSON_ARRAY_BEGIN, NULL, 0);
        for (int j = 0; j < sb_count(turnip->predictions[i]); j++)
        {
            json_print_pretty(&jp, JSON_OBJECT_BEGIN, NULL, 0);

            _json_format_key_str(&jp, PREDICTION_KEYS[PREDICTION_JSON_KEY_PATTERN_NAME],
                                      turnip->predictions[i][j]->pattern_name);

            _json_format_key(&jp, PREDICTION_KEYS[PREDICTION_JSON_KEY_PERIOD]);
            json_print_pretty(&jp, JSON_ARRAY_BEGIN, NULL, 0);
            for (int k = 0; k < 14; k++)
            {
                json_print_pretty(&jp, JSON_OBJECT_BEGIN, NULL, 0);
                _json_format_key_uint(&jp, PRICE_PERIOD_KEYS[PRICE_PERIOD_JSON_KEY_MIN],
                                      turnip->predictions[i][j]->period[k].min);
                _json_format_key_uint(&jp, PRICE_PERIOD_KEYS[PRICE_PERIOD_JSON_KEY_MAX],
                                      turnip->predictions[i][j]->period[k].max);
                json_print_pretty(&jp, JSON_OBJECT_END, NULL, 0);
            }
            json_print_pretty(&jp, JSON_ARRAY_END, NULL, 0);

            _json_format_key_uint(&jp, PREDICTION_KEYS[PREDICTION_JSON_KEY_MIN],
                                       turnip->predictions[i][j]->min);
            _json_format_key_uint(&jp, PREDICTION_KEYS[PREDICTION_JSON_KEY_MAX],
                                       turnip->predictions[i][j]->max);
            json_print_pretty(&jp, JSON_OBJECT_END, NULL, 0);
        }
        json_print_pretty(&jp, JSON_ARRAY_END, NULL, 0);
    }
    json_print_pretty(&jp, JSON_ARRAY_END, NULL, 0);

    json_print_pretty(&jp, JSON_OBJECT_END, NULL, 0);
    json_print_free(&jp);

    return buf;
}


static int
_turnip_print_json_callback(void* data,
                            const char* s,
                            attr_unused uint32_t len)
{
    char* buf = (char*)data;
    strncat(buf, s, len);
    return 1;
}

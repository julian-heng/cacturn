#ifndef TURNIPS_H
#define TURNIPS_H


#define TURNIP_PREDICTION_RANGE "%03d-%03d"
#define TURNIP_PREDICTION_DAY_FORMAT \
    TURNIP_PREDICTION_RANGE " " TURNIP_PREDICTION_RANGE
#define TURNIP_PREDICTION_WEEK_FORMAT \
    "%s\t" \
    TURNIP_PREDICTION_DAY_FORMAT "\t" \
    TURNIP_PREDICTION_DAY_FORMAT "\t" \
    TURNIP_PREDICTION_DAY_FORMAT "\t" \
    TURNIP_PREDICTION_DAY_FORMAT "\t" \
    TURNIP_PREDICTION_DAY_FORMAT "\t" \
    TURNIP_PREDICTION_DAY_FORMAT "\t" \
    TURNIP_PREDICTION_RANGE

#define FILTER_NONE 0x0
#define FILTER_ALL 0x0F
#define FILTER_ACTIVE 0x10
#define FILTER_FLUCTUATING 0x01
#define FILTER_LARGE_SPIKE 0x02
#define FILTER_DECREASING 0x04
#define FILTER_SMALL_SPIKE 0x08

#define set_next_pattern_from_prob(n, p1, p2, p3) \
    ((n) < (p1) ? 0 : (n) < (p2) ? 1 : (n) < (p3) ? 2 : 3)
#define min_rate_from_sell_buy(s, b) (10000 * ((s) - 1) / (b))
#define max_rate_from_sell_buy(s, b) (10000 * (s) / (b))


enum days
{
    SUNDAY_AM    = 0,
    SUNDAY_PM    = 1,
    MONDAY_AM    = 2,
    MONDAY_PM    = 3,
    TUESDAY_AM   = 4,
    TUESDAY_PM   = 5,
    WEDNESDAY_AM = 6,
    WEDNESDAY_PM = 7,
    THURSDAY_AM  = 8,
    THURSDAY_PM  = 9,
    FRIDAY_AM    = 10,
    FRIDAY_PM    = 11,
    SATURDAY_AM  = 12,
    SATURDAY_PM  = 13
};


enum patterns
{
    PATTERN_FLUCTUATING,
    PATTERN_LARGE_SPIKE,
    PATTERN_DECREASING,
    PATTERN_SMALL_SPIKE,
    NUM_PATTERNS
};


enum price_period_json_keys
{
    PRICE_PERIOD_JSON_KEY_MIN,
    PRICE_PERIOD_JSON_KEY_MAX
};


enum prediction_json_keys
{
    PREDICTION_JSON_KEY_PATTERN_NAME,
    PREDICTION_JSON_KEY_PERIOD,
    PREDICTION_JSON_KEY_MIN,
    PREDICTION_JSON_KEY_MAX
};


enum turnip_json_keys
{
    TURNIP_JSON_KEY_BASE_PRICE,
    TURNIP_JSON_KEY_SELL_PRICE,
    TURNIP_JSON_KEY_PATTERN,
    TURNIP_JSON_KEY_PREDICTIONS
};


typedef struct price_period_t
{
    uint16_t min;
    uint16_t max;
} price_period_t;


typedef struct prediction_t
{
    char pattern_name[12];
    price_period_t period[14];
    uint16_t min;
    uint16_t max;
} prediction_t;


typedef struct turnip_price_t
{
    uint8_t base_price;
    int16_t sell_price[14];
    uint8_t pattern;
    prediction_t** predictions[NUM_PATTERNS]; // stretchy buffer
} turnip_price_t;


typedef enum operation_mode
{
    OPERATE_QUERY,
    OPERATE_GENERATE
} operation_mode;


typedef enum view_mode
{
    VIEW_NORMAL,
    VIEW_JSON
} view_mode;


typedef struct options_t
{
    operation_mode op;
    view_mode view;
    uint8_t pattern;
} options_t;


void usage(char* prog);
void parse_options(int argc, char** argv, options_t* opt);

turnip_price_t* turnip_new(void);
void turnip_destroy(turnip_price_t** _turnip);
void turnip_calculate_price(turnip_price_t* turnip, sead_random_t* rand);
void turnip_setup_next_price(turnip_price_t* turnip, sead_random_t* rand);
void turnip_calculate_pattern_0(turnip_price_t* turnip, sead_random_t* rand);
void turnip_calculate_pattern_1(turnip_price_t* turnip, sead_random_t* rand);
void turnip_calculate_pattern_2(turnip_price_t* turnip, sead_random_t* rand);
void turnip_calculate_pattern_3(turnip_price_t* turnip, sead_random_t* rand);

void turnip_generate_pattern_0(turnip_price_t* turnip);
void turnip_generate_pattern_1(turnip_price_t* turnip);
void turnip_generate_pattern_2(turnip_price_t* turnip);
void turnip_generate_pattern_3(turnip_price_t* turnip);

void turnip_print_prices(turnip_price_t* turnip);
void turnip_print_predictions(turnip_price_t* turnip);
void turnip_print_json(turnip_price_t* turnip);


#endif

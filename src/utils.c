#include <string.h>
#include <stdio.h>
#include <stdbool.h>

#include "../lib/libjson/json.h"
#include "../lib/cresead/src/cresead.h"
#include "utils.h"


void
_json_format_key(json_printer* jp, const char* k)
{
    json_print_pretty(jp, JSON_KEY, k, strlen(k));
}


void
_json_format_key_str(json_printer* jp, const char* k, const char* v)
{
    _json_format_key(jp, k);
    json_print_pretty(jp, JSON_STRING, v, strlen(v));
}


void
_json_format_key_int(json_printer* jp, const char* k, int v)
{
    char* v_str = intstr(v);
    _json_format_key(jp, k);
    json_print_pretty(jp, JSON_INT, v_str, strlen(v_str));
}


void
_json_format_key_uint(json_printer* jp, const char* k, unsigned int v)
{
    char* v_str = uintstr(v);
    _json_format_key(jp, k);
    json_print_pretty(jp, JSON_INT, v_str, strlen(v_str));
}


void
_json_format_key_int16_arr(json_printer* jp,
                           const char* k,
                           int16_t* v,
                           size_t nmemb)
{
    char* v_str;
    _json_format_key(jp, k);
    json_print_pretty(jp, JSON_ARRAY_BEGIN, NULL, 0);

    for (size_t i = 0; i < nmemb; i++)
    {
        v_str = intstr(v[i]);
        json_print_pretty(jp, JSON_INT, v_str, strlen(v_str));
    }

    json_print_pretty(jp, JSON_ARRAY_END, NULL, 0);
}


char*
intstr(int n)
{
    static char buf[_INTMAX_LENGTH];
    snprintf(buf, _INTMAX_LENGTH, "%d", n);
    return buf;
}


char*
uintstr(unsigned int n)
{
    static char buf[_UINTMAX_LENGTH];
    snprintf(buf, _UINTMAX_LENGTH, "%u", n);
    return buf;
}


bool
bit_set(unsigned int v, unsigned int n)
{
    return n <= 0 ? false : (v >> (n - 1)) & 1U;
}


bool
randbool(sead_random_t* rand)
{
    return (bool)(sead_random_get_u32(rand) & 0x80000000);
}


int
randint(sead_random_t* rand, int min, int max)
{
    return (((uint64_t)sead_random_get_u32(rand) * (uint64_t)(max - min + 1)) >> 32) + min;
}


float
randfloat(sead_random_t* rand, float a, float b)
{
    uint32_t tmp = 0x3F800000 | (sead_random_get_u32(rand) >> 9);
    float ftmp = *(float*)(&tmp);
    return a + ((ftmp - 1.0f) * (b - a));
}


int
intceil(float val)
{
    return (int)(val + 0.99999f);
}

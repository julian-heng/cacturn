#ifndef UTILS_H
#define UTILS_H

#define _UINTMAX_LENGTH 20
#define _INTMAX_LENGTH 20

#define out_of_range(n, a, b) ((n) < (a) || (n) > (b))
#define attr_unused __attribute__((unused))

void _json_format_key(json_printer* jp, const char* k);
void _json_format_key_str(json_printer* jp, const char* k, const char* v);
void _json_format_key_int(json_printer* jp, const char* k, int v);
void _json_format_key_uint(json_printer* jp, const char* k, unsigned int v);
void _json_format_key_int16_arr(json_printer* jp,
                                const char* k,
                                int16_t* v,
                                size_t nmemb);

char* intstr(int n);
char* uintstr(unsigned int n);

bool bit_set(unsigned int v, unsigned int n);

bool randbool(sead_random_t* rand);
int randint(sead_random_t* rand, int min, int max);
float randfloat(sead_random_t* rand, float a, float b);
int intceil(float val);

#endif
